package restservice.finanseApp_backend;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.DataInput;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(VersionController.class)
class VersionControllerTest {

    @Autowired
    private MockMvc mvc;
    @Value("${api.version}")
    private String version;

    @Test
    void version() throws Exception {

        RequestBuilder request = MockMvcRequestBuilders.get("/api/version");
        MvcResult result = mvc.perform(request).andReturn();
        JSONObject json = new JSONObject(result.getResponse().getContentAsString());
        assertEquals(version, json.get("version"));
    }
}