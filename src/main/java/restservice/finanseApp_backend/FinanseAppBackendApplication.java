package restservice.finanseApp_backend;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class FinanseAppBackendApplication {
	@Value("${api.pattern}")
	String pathPattern;
	@Value("${api.host}")
	String host;
	String testingCache = "test";

	public static void main(String[] args) {
		SpringApplication.run(FinanseAppBackendApplication.class, args);
	}
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping(pathPattern).allowedOrigins(host);
			}
		};
	}

}
