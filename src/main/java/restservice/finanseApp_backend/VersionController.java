package restservice.finanseApp_backend;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {
    @Value("${api.version}")
    String version;

    @GetMapping("/api/version")
    public Version version(){
        return new Version(version);
    }

}
